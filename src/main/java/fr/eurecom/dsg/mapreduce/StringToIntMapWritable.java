package fr.eurecom.dsg.mapreduce;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


import org.apache.hadoop.io.Writable;
/*
 * Very simple (and scholastic) implementation of a Writable associative array for String to Int
 *
 **/
public class StringToIntMapWritable implements Writable {
    private HashMap<String, Integer> hm = new HashMap<String, Integer>();

    // TODO: add an internal field that is the real associative array

    @Override
    public void readFields(DataInput in) throws IOException {
        int len = in.readInt();
        hm.clear();
        for (int i = 0; i < len; i++) {
            int l = in.readInt();
            byte[] ba = new byte[l];
            in.readFully(ba);
            String key = new String(ba);
            Integer value = in.readInt();
            hm.put(key, value);

            // TODO: implement deserialization

            // Warning: for efficiency reasons, Hadoop attempts to re-use old instances of
            // StringToIntMapWritable when reading new records. Remember to initialize your variables
            // inside this function, in order to get rid of old data.

        }
    }

    public StringToIntMapWritable() {
    }

    public StringToIntMapWritable(String key, Integer value) {
        hm.put(key, value);
    }

    public List<StringToIntMapWritable> getElements() {
        List<StringToIntMapWritable> result = new ArrayList<StringToIntMapWritable>();
        for (String key : hm.keySet()) {
            result.add(new StringToIntMapWritable(key, hm.get(key)));
        }
        return result;
    }

    public Set<String> getKeySet() {
        return hm.keySet();
    }

    public Integer getValue(String key) {
        return hm.get(key);
    }

    @Override
    public String toString() {
        return hm.keySet().iterator().next() + " " + hm.values().iterator().next();
    }


    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(hm.size());
        Iterator<Entry<String, Integer>> it = hm.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>) it
                    .next();
            String k = (String) pairs.getKey();
            Integer v = (Integer) pairs.getValue();
            out.writeInt(k.length());
            out.writeBytes(k);
            out.writeInt(v);

            // TODO: implement serialization
        }
    }

    public void clear() {
        hm.clear();
    }

    public void increment(String t) {
        int count = 1;
        if (hm.containsKey(t)) {
            count = hm.get(t) + count;
        }
        hm.put(t, count);
    }

    public void increment(String t, int value) {
        int count = value;
        if (hm.containsKey(t)) {
            count = hm.get(t) + count;
        }
        hm.put(t, count);
    }

    public void sum(StringToIntMapWritable h) {
        Iterator<Entry<String, Integer>> it = h.hm.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>) it
                    .next();
            String k = (String) pairs.getKey();
            Integer v = (Integer) pairs.getValue();
            increment(k, v);
        }
    }
}
