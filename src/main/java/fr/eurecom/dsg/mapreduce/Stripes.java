package fr.eurecom.dsg.mapreduce;

import java.io.IOException;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Stripes extends Configured implements Tool {

    private int numReducers;
    private Path inputPath;
    private Path outputDir;

    @Override
    public int run(String[] args) throws Exception {

        Configuration conf = this.getConf();
        Job job = new Job(conf, "Stripes");

        // set job input format
        job.setInputFormatClass(TextInputFormat.class);

        // set map class and the map output key and value classes
        job.setMapperClass(StripesMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(StringToIntMapWritable.class);

        // set reduce class and the reduce output key and value classes
        job.setReducerClass(StripesReducer.class);

        // set job output format
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(StringToIntMapWritable.class);

        // add the input file as job input (from HDFS) to the variable
        // inputFile
        FileInputFormat.addInputPath(job, inputPath);

        // set the output path for the job results (to HDFS) to the
        // variable
        // outputPath
        FileOutputFormat.setOutputPath(job, outputDir);

        // set the number of reducers using variable numberReducers
        job.setNumReduceTasks(numReducers);

        // set the jar class
        job.setJarByClass(Stripes.class);


        return job.waitForCompletion(true) ? 0 : 1;
    }

    public Stripes(String[] args) {
        if (args.length != 3) {
            System.out
                    .println("Usage: Stripes <num_reducers> <input_path> <output_path>");
            System.exit(0);
        }
        this.numReducers = Integer.parseInt(args[0]);
        this.inputPath = new Path(args[1]);
        this.outputDir = new Path(args[2]);
    }

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new Stripes(args), args);
        System.exit(res);
    }
}

class StripesMapper extends
        Mapper<LongWritable, Text, Text, StringToIntMapWritable> {

    @Override
    public void map(LongWritable key, // input key type
                    Text value, // input value type
                    Context context) throws java.io.IOException, InterruptedException {

        String line = value.toString();
        line = line.replaceAll("[^a-zA-Z0-9_]+", " ");
        line = line.replaceAll("^\\s+", "");
        String[] tokens = line.split("\\s+");
        StringToIntMapWritable h = new StringToIntMapWritable();
        for (int i = 0; i < tokens.length - 1; i++) {
            h.clear();
			/*for (int j = Math.max(0, i - 1); j < Math.min(tokens.length, i + 2); j++) {
				// search three element: previous, current, next ? why search previous 
				if (i == j)
					// skip current element
					continue;
				h.increment(tokens[j]);
			}*/
            String left = tokens[i];
            String right = tokens[i+1];
            h.increment(right);

            context.write(new Text(left), h);
        }
    }
}

class StripesReducer extends Reducer<Text, // input key type
        StringToIntMapWritable, // input value type
        Text, // output key type
        StringToIntMapWritable> { // output value type
    @Override
    public void reduce(Text key, // input key type
                       Iterable<StringToIntMapWritable> values, // input value type
                       Context context) throws IOException, InterruptedException {

        StringToIntMapWritable hf = new StringToIntMapWritable();
        for (StringToIntMapWritable value : values) {
            hf.sum(value);
        }

        for (StringToIntMapWritable item : hf.getElements()) {
            context.write(key, item);
        }

    }
}
